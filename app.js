const app = require('express')()
const config = require('./config')
// const api = require('./api')

const server = app.listen(config.port, () => {

    console.log(`Server running - ${config.port}.`)

    config.middleware(app)
    // api.registerRoutes(app)

    // app.get('*', (req, res) => {
        // return res.sendFile(path.resolve('../data/client/index.html'))
        // res.json({haworth: "justin"})
    // })
})