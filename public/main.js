const total = 5
const suits = ['s','h','c','d']
const cards = 13

const types = {
    straightFlush: {
        rank: 1,
        sort: (a, b) => {
            return getStraightHighestCard(b, false) - getStraightHighestCard(a, false)
        },
        display: h => {

            const val = getStraightHighestCard(h, true)

            if(val == 'Ace') {
                return 'Royal Flush'
            }

            return `${val} High Straight Flush`
        },
        sortCardsForDisplay: h => straightSort(h)
    },
    fourKind: {
        rank: 2,
        sort: (a, b) => {

            const av = getFourOfKindVal(a, false)
            const bv = getFourOfKindVal(b, false)

            if(av != bv) {
                return bv - av
            }else{
                return getFourKindExtra(b) - getFourKindExtra(a)
            }
        },
        display: h => {
            const high = fixWordForPlural(getFourOfKindVal(h, true))
            return `Four ${high}`
        },
        sortCardsForDisplay: h => {
            return h
        }
    },
    fullHouse: {
        rank: 3,
        sort: (a, b) => {

            let aval = getFullHouseVal(a, true, false)
            let bval = getFullHouseVal(b, true, false)

            if(aval == bval) {
                aval = getFullHouseVal(a, false, false)
                bval = getFullHouseVal(b, false, false)
            }

            return bval - aval
        },
        display: h => {
            const high = fixWordForPlural(getFullHouseVal(h, true, true))
            const low = fixWordForPlural(getFullHouseVal(h, false, true))
            return `${high} Over ${low}`
        },
        sortCardsForDisplay: h => {

            const hv = getFullHouseVal(h, true, false)
            const lv = getFullHouseVal(h, false, false)
            const high = fixWordForPlural(hv)
            const low = fixWordForPlural(lv)
            const top = h.filter(c => cardValue(c, true, false, false) == hv)
            const bot = h.filter(c => cardValue(c, true, false, false) == lv)

            return [...top, ...bot]
        }
    },
    flush: {
        rank: 4,
        sort: (a, b) => {
            return sortNoPair(a, b)
        },
        display: h => {

            // h = convertHandAcesToFourteens(h)
            h = sortHandNumerically(h)

            const high = cardValue(h[4], true, true, true)
            return `${high} High Flush`
        },
        sortCardsForDisplay: h => {

            // h = convertHandAcesToFourteens(h)
            h = sortHandNumerically(h)

            return h.reverse()
        }
    },
    straight: {
        rank: 5,
        sort: (a, b) => {
            return getStraightHighestCard(b, false) - getStraightHighestCard(a, false)
        },
        display: h => {
            const high = getStraightHighestCard(h, true)
            return `${high} High Straight`
        },
        sortCardsForDisplay: h => straightSort(h)
    },
    threeKind: {
        rank: 6,
        sort: (a, b) => {

            let av = getThreeOfKindVal(a, false)
            let bv = getThreeOfKindVal(b, false)

            if(av != bv) {
                return bv - av
            }else{

                const remA = getThreeKindRemainingCards(a, false)
                const remB = getThreeKindRemainingCards(b, false)

                return sortNoPair(remA, remB)
            }
        },
        display: h => {
            return `Three ${fixWordForPlural(getThreeOfKindVal(h, true))}`
        },
        sortCardsForDisplay: h => {

            let sorted = []
            let tok = getThreeOfKindVal(h, false)

            // h = convertHandAcesToFourteens(h)
            h = sortHandNumerically(h)

            let rem = getThreeKindRemainingCards(h, true)

            h.forEach(c => {
                if(Number(c.slice(1, c.length)) == tok) {
                    sorted.push(c)
                }
            })

            sorted = sorted.concat(rem)

            return sorted
        }
    },
    twoPair: {
        rank: 7,
        sort: (a, b) => {

            let aval = getTwoPairVal(a, true, false, false)
            let bval = getTwoPairVal(b, true, false, false)

            if(aval == bval) {
                aval = getTwoPairVal(a, false, false, false)
                bval = getTwoPairVal(b, false, false, false)
            }

            if(aval == bval) {
                aval = getTwoPairVal(a, false, true, false)
                bval = getTwoPairVal(b, false, true, false)
            }

            return bval - aval
        },
        display: h => {

            const hp = fixWordForPlural(getTwoPairVal(h, true, false, true))
            const lp = fixWordForPlural(getTwoPairVal(h, false, false, true))

            return `Two Pair ${hp} & ${lp}`
        },
        sortCardsForDisplay: h => {

            const hp = getTwoPairVal(h, true, false, false)
            const lp = getTwoPairVal(h, false, false, false)
            const high = h.filter(c => cardValue(c, true, false, false) == hp)
            const low = h.filter(c => cardValue(c, true, false, false) == lp)
            const other = getTwoPairVal(h, false, true, false)
            
            let otherCard

            h.forEach(c => {
                if(cardValue(c, true, false, false) == other) {
                    otherCard = c
                }
            })

            return [...high, ...low, otherCard]
        }
    },
    pair: {
        rank: 8,
        sort: (a, b) => {

            let aval = getPairVal(a, false)
            let bval = getPairVal(b, false)

            // console.log('aval', aval)
            // console.log('bval', bval)

            if(aval == bval) {

                const remA = getPairRemainingCards(a, false)
                const remB = getPairRemainingCards(b, false)

                // console.log('remA', remA)
                // console.log('remB', remB)

                return sortNoPair(remA, remB)

            }else{
                return bval - aval
            }
        },
        display: h => {

            const val = fixWordForPlural(getPairVal(h, true))

            return `Pair of ${val}`
        },
        sortCardsForDisplay: h => {

            let sorted = []
            const pair = getPairVal(h, false)

            // h = convertHandAcesToFourteens(h)
            h = sortHandNumerically(h)

            let rem = getPairRemainingCards(h, true)

            h.forEach(c => {
                if(Number(c.slice(1, c.length)) == pair) {
                    sorted.push(c)
                }
            })

            sorted = sorted.concat(rem)

            return sorted
        }
    },
    highCard: {
        rank: 9,
        sort: (a, b) => {
            return sortNoPair(a, b)
        },
        display: h => {
            
            let highestCard = cardValue(h[4], false, false, false) == 14 ? 'Ace' : cardValue(h[4], false, true, true)

            return `${highestCard} High`
        },
        sortCardsForDisplay: h => {
            // h = convertHandAcesToFourteens(h)
            h = sortHandNumerically(h)
            return h.reverse()
        }
    }
}

function straightSort(h) {

    const hasAce = cardValue(h[4], false, false, false) == 14

    if(hasAce) {

        const aceIsHighEndOfStraight = isAceHighEnd(h)

        if(!aceIsHighEndOfStraight) {
            // h = convertHandAcesToFourteens(h)
            h = convertHandAcesToOnes(h)
        }
    }
    
    h = sortHandNumerically(h)

    return h.reverse()
}

function genDeck() {

    const d = []

    suits.forEach(s => {
        for(let i = 2; i <= cards + 1; ++i) {
            d.push(`${s+i}`)
        }
    })

    return d
}

function genHands() {

    let h = []

    for(let i = 0; i < total; ++i) {
        h.push(genHand())
    }

    return h
}

function genHand() {

    let hand = []
    let myDeck = deck.slice(0)

    while(hand.length < 5) {
        const rand = Math.floor(Math.random() * myDeck.length)
        hand.push(myDeck.splice(rand, 1)[0])
    }

    return hand
}

function genSortableHands(hands) {

    const ret = []

    hands.forEach(hand => {

        const type = determineHandType(hand)

        ret.push({
            type,
            rank: types[type].rank,
            display: types[type].display(hand),
            hand
        })
    })

    return ret
}

function sortHands(hands) {
    return hands.sort((a, b) => {
        if(a.rank != b.rank) {
            return a.rank - b.rank
        }else{
            return types[a.type].sort(a.hand, b.hand)
        }
    })
}

function determineHandType(hand) {

    // console.log(hand)

    const suits = {}
    const counts = {}
    let numbSuits = 0
    let numbCounts = 0

    hand.forEach(c => {

        const suit = c.slice(0, 1)
        const count = c.slice(1, c.length)

        if(!suits[suit]) {
            ++numbSuits
            suits[suit] = 0
        }

        if(!counts[count]) {
            ++numbCounts
            counts[count] = 0
        }

        ++suits[suit]
        ++counts[count]
    })

    if(numbCounts < 5) {

        let highest = 2
        let moreThanTwo = 0

        for(let prop in counts) {

            const val = counts[prop]

            if(val > highest) {
                highest = val
            }

            if(val > 1) {
                ++moreThanTwo
            }

        }

        if(moreThanTwo == 2) {
            return highest == 3 ? 'fullHouse' : 'twoPair'
        }else{
            switch(highest) {
                case 4:
                    return 'fourKind'
                    break
                case 3:
                    return 'threeKind'
                    break
                default:
                    return 'pair'
            }
        }
    }else{

        const straight = isStraight(hand)
        const flush = numbSuits == 1

        if(straight && flush) {
            return 'straightFlush'
        }else if(flush) {
            return 'flush'
        }else if(straight) {
            return 'straight'
        }

        return 'highCard'
    }
}

function isStraight(hand) {

    const hasAce = cardValue(hand[4], false, false, false) == 14

    if(hasAce) {
        return cardValue(hand[0], false, false, false) == 10 || cardValue(hand[3], false, false, false) == 5
    }else{
        return cardValue(hand[4], false, false, false) - cardValue(hand[0], false, false, false) == 4
    }
}

function getStraightHighestCard(hand, returnName) {
    // console.log(hand)

    const c4 = cardValue(hand[3], false, false, false)
    const c4n = cardValue(hand[3], false, true, true)
    const c5 = cardValue(hand[4], false, false, false)
    const c5n = cardValue(hand[4], false, true, true)

    // console.log('c4', c4)
    // console.log('c5', c5)

    if(c4 == 5 && c5 == 14) {
        return returnName ? c4n : c4
    }

    return returnName ? c5n : c5

    // const hasAce = cardValue(hand[4], false, false, false) == 14

    // if(hasAce) {

    //     const aceIsHighEndOfStraight = isAceHighEnd(hand)

    //     if(returnName) {
    //         return aceIsHighEndOfStraight ? cardValue(hand[0], false, true, true) : cardValue(hand[4], false, true, true)
    //     }

    //     return aceIsHighEndOfStraight ? 14 : 5

    // }else{
        
    //     if(returnName) {
    //         return cardValue(hand[4], false, true, true)
    //     }

    //     return cardValue(hand[4], false, false, false)
    // }
}

function isAceHighEnd(hand) {
    return cardValue(hand[0], false, false, false) == 10
}

function getFourOfKindVal(hand, convertBigNames) {
    let cv = cardValue(hand[1], true, convertBigNames, convertBigNames)
    return cv
}

function getThreeOfKindVal(hand, convertBigNames) {
    return cardValue(hand[2], true, convertBigNames, convertBigNames)
}

function getPairVal(hand, convertBigNames) {

    let pair
    let i = 0

    while(!pair) {

        const plus = i + 1

        if(hand[plus] && cardValue(hand[i], false, false, false) == cardValue(hand[plus], false, false, false)) {
            pair = cardValue(hand[i], true, convertBigNames, convertBigNames)
        }

        ++i
    }

    return pair
}

function getPairRemainingCards(hand, rev) {

    const pv = getPairVal(hand)
    const ret = []

    hand.forEach(i => {

        const v = cardValue(i, false, false, false)

        if(v != pv) {
            ret.push(i)
        }
    })

    if(rev) {
        ret.reverse()
    }

    return ret
}

function getThreeKindRemainingCards(hand, rev) {

    const tv = getThreeOfKindVal(hand, false)
    const ret = []

    hand.forEach(i => {

        const v = cardValue(i, false, false, false)

        if(v != tv) {
            ret.push(i)
        }
    })

    if(rev) {
        ret.reverse()
    }

    return ret
}

function getFourKindExtra(hand) {

    const a = cardValue(hand[0], true, false, false)
    const b = cardValue(hand[1], true, false, false)
    const c = cardValue(hand[4], true, false, false)

    return a == b ? c : a
}

function getFullHouseVal(hand, over, convertBigNames) {
    
    const a = cardValue(hand[1], true, convertBigNames, convertBigNames)
    const b = cardValue(hand[2], true, convertBigNames, convertBigNames)
    const c = cardValue(hand[3], true, convertBigNames, convertBigNames)

    if(a == b) {
        return over ? a : c
    }else{
        return over ? b : a
    }
}

function getTwoPairVal(hand, top, nonpair, convertBigNames) {
    
    const c1 = cardValue(hand[0], true, convertBigNames, convertBigNames)
    const c2 = cardValue(hand[1], true, convertBigNames, convertBigNames)
    const c3 = cardValue(hand[2], true, convertBigNames, convertBigNames)
    const c4 = cardValue(hand[3], true, convertBigNames, convertBigNames)
    const c5 = cardValue(hand[4], true, convertBigNames, convertBigNames)

    if(nonpair) {
        if(c1 == c2) {
            return c3 == c4 ? c5 : c3
        }else{
            return c1
        }
    }

    if(convertBigNames) {
        return top ? c2 == 'Ace' ? c2 : c4 : c2 == 'Ace' ? c4 : c2
    }else{
        return top ? c2 == 14 ? c2 : c4 : c2 == 14 ? c4 : c2
    }
}

function sortNoPair(a, b) {

    // console.log('sort', a, b)

    let aval = 0
    let bval = 0
    let i = a.length - 1

    while(i >= 0) {

        const av = cardValue(a[i], true, false, false)
        const bv = cardValue(b[i], true, false, false)

        if(av != bv && aval == 0) {
            aval = av
            bval = bv
        }

        --i
    }

    // console.log('return', bval, aval)

    return bval - aval
}

function convertHandAcesToOnes(hand) {

    let ret = []

    hand.forEach(i => {
        if(cardValue(i, false, false, false) == 14) {
            ret.push(i.charAt(0) + 1)
        }else{
            ret.push(i)
        }
    })

    return ret
}

// function convertHandAcesToFourteens(hand) {

//     let ret = []

//     hand.forEach(i => {
//         if(cardValue(i, false, false, false) == 1) {
//             ret.push(i.charAt(0) + 14)
//         }else{
//             ret.push(i)
//         }
//     })

//     return ret
// }

function sortHandNumerically(h) {
    return h.sort((a, b) => cardValue(a, false, false, false) - cardValue(b, false, false, false))
}

function cardValue(c, convertAceToFourteen, convertBigNames, convertNumbers) {

    let val = Number(c.slice(1, c.length))

    // if(convertAceToFourteen && val == 1) {
    //     val = 14
    // }

    if(convertBigNames) {
        switch(val) {
            // case 1:
            //     val = 'Ace'
            //     break
            case 11:
                val = 'Jack'
                break
            case 12:
                val = 'Queen'
                break
            case 13:
                val = 'King'
                break
            case 14:
                val = 'Ace'
                break
        }
    }

    if(convertNumbers) {
        switch(val) {
            case 2:
                val = 'Two'
                break
            case 3:
                val = 'Three'
                break
            case 4:
                val = 'Four'
                break
            case 5:
                val = 'Five'
                break
            case 6:
                val = 'Six'
                break
            case 7:
                val = 'Seven'
                break
            case 8:
                val = 'Eight'
                break
            case 9:
                val = 'Nine'
                break
            case 10:
                val = 'Ten'
                break
        }
    }

    return val
}

function fixWordForPlural(word) {
    const fixes = {
        Six : 'Sixes'
    }
    return fixes[word] || `${word}s`
}

function getCardDisplaySuit(c) {

    const s = c.charAt(0)

    switch(s) {
        case 's':
            return 'Spades'
            break
        case 'h':
            return 'Hearts'
            break
        case 'c':
            return 'Clubs'
            break
        case 'd':
            return 'Diamonds'
            break
    }
}

function addHandsToBody(hands) {

    const wrapper = document.createElement('div')

    wrapper.className = 'wrapper'

    const mainTitle = document.createElement('h2')

    wrapper.appendChild(mainTitle)

    mainTitle.innerHTML = 'Poker Hand Generanker'

    const rawHandsTitle = document.createElement('div')
    
    rawHandsTitle.className = 'title'
    rawHandsTitle.innerHTML = 'Random Hand Data:'

    wrapper.appendChild(rawHandsTitle)

    const rawHands = document.createElement('div')

    rawHands.className = 'rawHands'

    handsUnsorted.forEach((h, i) => {

        const d = document.createElement('div')
        const j = i + 1

        d.className = 'rawHand'
        d.innerHTML = j + ') ' + h.join(',').replace(/,/g, ', ')

        rawHands.appendChild(d)
    })

    wrapper.appendChild(rawHands)

    const handsTitle = document.createElement('div')

    handsTitle.className = 'title'

    handsTitle.innerHTML = 'Sorted Hands:'

    wrapper.appendChild(handsTitle)

    hands.forEach(h => {
        
        const thisHand = types[h.type].sortCardsForDisplay(h.hand)

        const d = document.createElement('div')
        const cards = document.createElement('div')
        const text = document.createElement('div')

        d.className = 'hand'
        cards.className = 'cards'
        text.className = 'text'

        thisHand.forEach(c => {

            const card = document.createElement('div')
            const cardInner = document.createElement('div')
            const suit = c.charAt(0)
            let cv = cardValue(c, false, false, false)

            if(cv == 1) {
                cv = 14
            }

            const isRed = suit == 'h' || suit == 'd'
            const imgBase = '/images/'
            const vImg = `${imgBase}${isRed ? 'red/' : 'black/'}${cv}.svg`
            const sImg = `${imgBase}/suits/${suit}.svg`

            const valImage = document.createElement('img')
            const suitImage = document.createElement('img')

            valImage.src = vImg
            suitImage.src = sImg

            card.className = `card`
            cardInner.className = 'cardInner'
            valImage.className = 'value'
            suitImage.className = 'suit'

            cardInner.appendChild(valImage)
            cardInner.appendChild(suitImage)
            card.appendChild(cardInner)
            cards.appendChild(card)
        })

        text.innerHTML = h.display

        d.appendChild(text)
        d.appendChild(cards)
        wrapper.appendChild(d)

        const content = document.getElementById('content')

        content.appendChild(wrapper)
        // document.body.appendChild(wrapper)
    })
}

function clearBody() {
    const content = document.getElementById('content')
    content.innerHTML = ''
}

const deck = genDeck()
let handsUnsorted

function go() {

    clearBody()

    if(handsUnsorted) {
        handsUnsorted.length = 0
    }
    
    handsUnsorted = genHands()

    // handsUnsorted = [
    //     ['d14', 'h14', 's7', 'c7', 'h11']
    // ]

    // handsUnsorted = [
    //     ['s10','s13','s14','s12','s11'],
    //     ['h6','h9','h8','h7','h10'],
    //     ['d9','c9','h9','s9','h13'],
    //     ['d6','c6','h6','s6','h12'],
    //     ['d9','c9','h9','s9','h10'],
    //     ['d14','d3','h14','s3','c14'],
    //     ['d14','d3','h14','s3','c3'],
    //     ['s9','s14','h14','c14','c9'],
    //     ['h14','h9','h5','h10','h8'],
    //     ['h12','h9','h5','h10','h8'],
    //     ['s3','s9','s14','s10','s7'],
    //     ['s10','s13','h14','s12','s11'],
    //     ['s8','s7','c9','d10','h11'],
    //     ['h8','s4','d6','s5','c7'],
    //     ['h14','s4','d3','s5','c2'],
    //     ['h6','s4','d3','s5','c2'],
    //     ['s14','h14','d13','c10','c14'],
    //     ['c14','h14','d14','s3','s9'],
    //     ['c12','h12','d12','s3','s9'],
    //     ['c7','h7','d7','s14','s9'],
    //     ['c7','h7','d7','s13','s9'],
    //     ['c13','d13','h14','c10','s14'],
    //     ['c13','d13','h10','c10','s14'],
    //     ['h10','d10','s13','c13','d6'],
    //     ['d3','s14','h10','d6','c3'],
    //     ['d3','s14','h10','d6','c14'],
    //     ['d3','h13','d8','d9','h3'],
    //     ['s13','s5','d10','c11','h12'],
    //     ['s13','s4','d10','c11','h12'],
    //     ['s14','s5','d10','c11','h12'],
    //     ['s14','s5','d9','c11','h12'],
    //     ['s2','s5','d10','c11','h12']
    // ]

    let hands = []

    handsUnsorted.forEach(h => {
        hands.push(sortHandNumerically(h))
    })

    const sortableHands = genSortableHands(hands)
    const sortedHands = sortHands(sortableHands)

    setTimeout(() => {
        addHandsToBody(sortedHands)
        // code by justinjameshaworth at gmail.com
    }, 10)

}