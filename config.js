const express = require('express')
const logger = require('morgan')
const path = require('path')
const fs = require('fs')
const publicPath = path.normalize(__dirname + '/public')

module.exports = {

    port: 4444,

    middleware: (app) => {
        return mw(app)
    }
}

function mw(app) {
    app.use(express.static(publicPath))
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*")
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
        next()
    })
    app.use(logger('dev'))
}